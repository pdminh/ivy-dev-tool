[Ivy]
16EC601233019A46 3.20 #module
>Proto >Proto Collection #zClass
Hs0 HomeProcess Big #zClass
Hs0 RD #cInfo
Hs0 #process
Hs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Hs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Hs0 @TextInP .resExport .resExport #zField
Hs0 @TextInP .type .type #zField
Hs0 @TextInP .processKind .processKind #zField
Hs0 @AnnotationInP-0n ai ai #zField
Hs0 @MessageFlowInP-0n messageIn messageIn #zField
Hs0 @MessageFlowOutP-0n messageOut messageOut #zField
Hs0 @TextInP .xml .xml #zField
Hs0 @TextInP .responsibility .responsibility #zField
Hs0 @RichDialogInitStart f0 '' #zField
Hs0 @RichDialogProcessEnd f1 '' #zField
Hs0 @PushWFArc f2 '' #zField
Hs0 @RichDialogProcessEnd f4 '' #zField
Hs0 @RichDialogMethodStart f5 '' #zField
Hs0 @RichDialogMethodStart f3 '' #zField
Hs0 @GridStep f7 '' #zField
Hs0 @PushWFArc f8 '' #zField
Hs0 @PushWFArc f6 '' #zField
Hs0 @RichDialogProcessEnd f9 '' #zField
Hs0 @GridStep f11 '' #zField
Hs0 @PushWFArc f12 '' #zField
Hs0 @PushWFArc f10 '' #zField
>Proto Hs0 Hs0 HomeProcess #zField
Hs0 f0 guid 16EC60123538FF90 #txt
Hs0 f0 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f0 method start() #txt
Hs0 f0 disableUIEvents true #txt
Hs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Hs0 f0 outParameterDecl '<> result;
' #txt
Hs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Hs0 f0 53 85 22 22 14 0 #rect
Hs0 f0 @|RichDialogInitStartIcon #fIcon
Hs0 f1 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f1 53 213 22 22 14 0 #rect
Hs0 f1 @|RichDialogProcessEndIcon #fIcon
Hs0 f2 expr out #txt
Hs0 f2 64 107 64 213 #arcP
Hs0 f4 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f4 181 213 22 22 14 0 #rect
Hs0 f4 @|RichDialogProcessEndIcon #fIcon
Hs0 f5 guid 16EC9F862574EC48 #txt
Hs0 f5 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f5 method switchLanguageEnglish() #txt
Hs0 f5 disableUIEvents false #txt
Hs0 f5 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Hs0 f5 outParameterDecl '<> result;
' #txt
Hs0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>switchLanguageEnglish()</name>
    </language>
</elementInfo>
' #txt
Hs0 f5 373 85 22 22 14 0 #rect
Hs0 f5 @|RichDialogMethodStartIcon #fIcon
Hs0 f3 guid 16EC9F87DEE65D56 #txt
Hs0 f3 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f3 method switchLanguageGermany() #txt
Hs0 f3 disableUIEvents false #txt
Hs0 f3 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Hs0 f3 outParameterDecl '<> result;
' #txt
Hs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>switchLanguageGermany()</name>
    </language>
</elementInfo>
' #txt
Hs0 f3 181 85 22 22 14 0 #rect
Hs0 f3 @|RichDialogMethodStartIcon #fIcon
Hs0 f7 actionDecl 'vn.ivy.support.devtool.Home.HomeData out;
' #txt
Hs0 f7 actionTable 'out=in;
' #txt
Hs0 f7 actionCode 'import ch.ivyteam.ivy.security.IUser;
import java.util.Locale;

//set language from user settings or application details
IUser sessionUser = ivy.session.getSessionUser();
ivy.session.setContentLocale(Locale.GERMANY);
ivy.session.setFormattingLocale(Locale.GERMANY);
sessionUser.setEMailLanguage(Locale.GERMANY);' #txt
Hs0 f7 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f7 174 148 36 24 20 -2 #rect
Hs0 f7 @|StepIcon #fIcon
Hs0 f8 expr out #txt
Hs0 f8 192 107 192 148 #arcP
Hs0 f6 expr out #txt
Hs0 f6 192 172 192 213 #arcP
Hs0 f9 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f9 373 213 22 22 14 0 #rect
Hs0 f9 @|RichDialogProcessEndIcon #fIcon
Hs0 f11 actionDecl 'vn.ivy.support.devtool.Home.HomeData out;
' #txt
Hs0 f11 actionTable 'out=in;
' #txt
Hs0 f11 actionCode 'import ch.ivyteam.ivy.security.IUser;
import java.util.Locale;

//set language from user settings or application details
IUser sessionUser = ivy.session.getSessionUser();
ivy.session.setContentLocale(Locale.ENGLISH);
ivy.session.setFormattingLocale(Locale.ENGLISH);
sessionUser.setEMailLanguage(Locale.ENGLISH);

' #txt
Hs0 f11 type vn.ivy.support.devtool.Home.HomeData #txt
Hs0 f11 366 148 36 24 20 -2 #rect
Hs0 f11 @|StepIcon #fIcon
Hs0 f12 expr out #txt
Hs0 f12 384 107 384 148 #arcP
Hs0 f10 expr out #txt
Hs0 f10 384 172 384 213 #arcP
>Proto Hs0 .type vn.ivy.support.devtool.Home.HomeData #txt
>Proto Hs0 .processKind HTML_DIALOG #txt
>Proto Hs0 -8 -8 16 16 16 26 #rect
>Proto Hs0 '' #fIcon
Hs0 f0 mainOut f2 tail #connect
Hs0 f2 head f1 mainIn #connect
Hs0 f3 mainOut f8 tail #connect
Hs0 f8 head f7 mainIn #connect
Hs0 f7 mainOut f6 tail #connect
Hs0 f6 head f4 mainIn #connect
Hs0 f5 mainOut f12 tail #connect
Hs0 f12 head f11 mainIn #connect
Hs0 f11 mainOut f10 tail #connect
Hs0 f10 head f9 mainIn #connect
