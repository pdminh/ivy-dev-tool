package vn.ivy.support.devtool.bean;

import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import ch.ivyteam.ivy.dialog.execution.DialogRuntime;
import ch.ivyteam.ivy.environment.Ivy;

import com.google.gson.Gson;

@SuppressWarnings("restriction")
public class DevToolBean {
	public void init() {

	}

	public void getManagedBeanInfo() {
		Ivy.session().loginSessionUser("", "");
		FacesContext ctx = FacesContext.getCurrentInstance();
		ExternalContext externalContext = ctx.getExternalContext();
		Map<String, Object> requestMap = externalContext.getRequestMap();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		Map<String, Object> applicationMap = externalContext.getApplicationMap();
		DialogRuntime dRuntime = null;
		Ivy.log().info("REQUEST");
		for (Map.Entry<String, Object> entry : requestMap.entrySet()) {
			if (entry.getValue().getClass() == DialogRuntime.class) {
				Ivy.log().info("###########################  " + entry.getKey());
				DialogRuntime dr = (DialogRuntime) entry.getValue();
				dRuntime = dr;
				Ivy.log().info(dr.getDialogName());
				Ivy.log().info(dr.getData().getClass());
				Ivy.log().info(dr.getData().getClass().isAnnotationPresent(ManagedBean.class));
				Ivy.log().info(dr.getTask().getName());
				Ivy.log().info(dr.getInstanceId());

				if (dr.getBpmError() != null) {
					Ivy.log().error("START");
					Ivy.log().error(dr.getBpmError());
					Ivy.log().error(dr.getBpmError().getMessage());
					Ivy.log().error(dr.getBpmError().getErrorMessage());
					Ivy.log().error(dr.getBpmError().getTechnicalCause());
					Ivy.log().error("END");
				}

			}else{
				Ivy.log().debug(entry.getKey() + " - " + entry.getValue().getClass());
			}
		}

//		Ivy.log().info("SESSION");
//		for (Map.Entry<String, Object> entry2 : sessionMap.entrySet()) {
//			if (entry2.getValue().getClass() == DialogRuntime.class) {
//				Ivy.log().info("###########################  " + entry2.getKey());
//				DialogRuntime dr = (DialogRuntime) entry2.getValue();
//				Ivy.log().info(dr.getDialogName());
//				Ivy.log().info(dr.getData().getClass());
//				Ivy.log().info(dr.getData().getClass().isAnnotationPresent(ManagedBean.class));
//				Ivy.log().info(dr.getTask().getName());
//				Ivy.log().info(dr.getInstanceId());
//
//				if (dr.getBpmError() != null) {
//					Ivy.log().error("START");
//					Ivy.log().error(dr.getBpmError());
//					Ivy.log().error(dr.getBpmError().getMessage());
//					Ivy.log().error(dr.getBpmError().getErrorMessage());
//					Ivy.log().error(dr.getBpmError().getTechnicalCause());
//					Ivy.log().error("END");
//				}
//
//			}else{
//				Ivy.log().debug(entry2.getKey() + " - " + entry2.getValue().getClass());
//			}
//		}
//
//		Ivy.log().info("APPLICATION");
//		for (Map.Entry<String, Object> entry3 : applicationMap.entrySet()) {
//			if (entry3.getValue().getClass() == DialogRuntime.class) {
//				Ivy.log().info("###########################  " + entry3.getKey());
//				DialogRuntime dr = (DialogRuntime) entry3.getValue();
//				Ivy.log().info(dr.getDialogName());
//				Ivy.log().info(dr.getData().getClass());
//				Ivy.log().info(dr.getData().getClass().isAnnotationPresent(ManagedBean.class));
//				Ivy.log().info(dr.getTask().getName());
//				Ivy.log().info(dr.getInstanceId());
//
//				if (dr.getBpmError() != null) {
//					Ivy.log().error("START");
//					Ivy.log().error(dr.getBpmError());
//					Ivy.log().error(dr.getBpmError().getMessage());
//					Ivy.log().error(dr.getBpmError().getErrorMessage());
//					Ivy.log().error(dr.getBpmError().getTechnicalCause());
//					Ivy.log().error("END");
//				}
//
//			}else{
//				Ivy.log().debug(entry3.getKey() + " - " + entry3.getValue().getClass());
//			}
//		}

		String message = "ha ha ha";
		returnData(dRuntime.getData());
	}

	public void returnData(Object object) {
		RequestContext request = RequestContext.getCurrentInstance();
		request.addCallbackParam("data", new Gson().toJson(object));
	}
}
